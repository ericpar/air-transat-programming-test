# Description

This is the Python source code that tries to answer the questions sent to [Eric Parent](mailto:eric@eparent.info) for the evaluation of his application to the OR developer role at Air Transat.


This module includes the files `question_1.py` and `question_2.py`, which try to answer the two questions of the programming test.

These files are located under `src`. The directory `test` contains unit tests for validating the program itself.


# Install dependencies

## Install Python

This code has been developed using Python 3.7.3. If you need to install such interpreter, please visit the official website at [Python.org](https://www.python.org/downloads/).

Download and install using the instructions available here.

## Installation of `pip`


The program `pip` is the Python packaging management system of the official distribution. It allows the easy retrieval and installation of open source Python packages.


## Virtual Environnements


### Installation of `virtualenv`

Typically, virtual environments utilities are installed using the following command:

```
$ pip install virtualenv
```

This should work on either Windows, Mac OS X or GNU/Linux systems.


### Création d'un environnement virtuel

From the root of the `src` directory, issue the following command in a directory named `Env`:

```
virtualenv venv
```

**Remarque**: the name of the virtual environment is arbitrary and could be named differently than, like `airtransat` for instance.

Once this is done, one can simply activate this virtual environment like this:

```
source venv/bin/activate
```

On Windows, one has to use this command:

```
venv\Scripts\activate
```


## Installation of dependencies in the virtual environment


Here is the chronological order of the steps to follow in order to install the required Python packages:

```
$ virtualenv venv
$ venv\Scrtipts\activate
$ pip install -r requirements.txt
$ python setup.py develop
```

# -*- coding: utf-8 -*-
from unittest import TestCase
import os

from src.question1 import (
    fetch_flight_info,
    next_class,
    compute_ratios,
    compute_booking_ratios
)


class Test_Question1(TestCase):

    def _get_canonical_ratios(self):
        ratios = {
            "Q": 200.0 / 120.0,
            "X": 270.0 / 200.0,
            "V": 900.0 / 270.0,
        }
        return ratios

    def _get_canonical_booking_ratio(self):
        infos = self._get_canonical_flight_info()
        total = 25.0
        infos["Q"]["booking ratio"] = (1.0 / total) * 100.0
        infos["X"]["booking ratio"] = (4.0 / total) * 100.0
        infos["V"]["booking ratio"] = (5.0 / total) * 100.0
        infos["Y"]["booking ratio"] = (15.0 / total) * 100.0
        return infos

    def _get_canonical_flight_info(self):
        infos = {
            'flight_id': 'YULCDG215136',
            'encountered fare classes': ['Q', 'X', 'V', 'Y'],
            'Q': {
                'fare class rank': 1,
                'fare value': 120.0,
                'booking': 1
            },
            'X': {
                'fare class rank': 2,
                'fare value': 200.0,
                'booking': 4
            },
            'V': {
                'fare class rank': 3,
                'fare value': 270.0,
                'booking': 5
            },
            'Y': {
                'fare class rank': 4,
                'fare value': 900.0,
                'booking': 15
            }
        }
        return infos


    def test_fetch_flight_info(self):
        '''Testing with unordered values.'''
        expected = self._get_canonical_flight_info()
        actual = fetch_flight_info("YULCDG215136")
        self.assertEqual(expected, actual)


    def test_next_class(self):
        self.assertEqual("X", next_class("Q"))
        self.assertEqual("V", next_class("X"))
        self.assertEqual("Y", next_class("V"))
        self.assertEqual(None, next_class("Y"))
        self.assertEqual(None, next_class("B"))


    def test_compute_ratios(self):
        expected = self._get_canonical_ratios()
        flight_infos = self._get_canonical_flight_info()
        infos = compute_ratios(flight_infos)
        for fare_class in ("Q", "X", "V"):
            self.assertEqual(
                expected[fare_class],
                infos[fare_class],
                msg="problem with ratio for fare class '{}'".format(fare_class)
            )


    def test_compute_ratios(self):
        expected = self._get_canonical_booking_ratio()
        actual = compute_booking_ratios(self._get_canonical_flight_info())
        self.assertEqual(expected, actual)


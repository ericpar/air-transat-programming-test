# -*- coding: iso-latin-1 -*-
from setuptools import setup, find_packages

__version__ = '0.0.1'

setup(
    name='air-transat-prog-test',
    version=__version__,
    description='Programming test for Air Transat',
    url='https://bitbucket.org/ericpar/visiopill',
    license='Proprietary',
    packages=find_packages(),
    #scripts=['scripts/visiopill.py'],
    install_requires=[
    ],
)

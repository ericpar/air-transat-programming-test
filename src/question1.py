#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
from collections import UserDict

# Assumptions
#
# 1 - based on the context of the problem, the table is simply too big to be
# stored in-memory. Therefore, one approach is to have all the queries to fetch
# data for the designated flight. It is time consuming but it is manageable.
#
# 2 - the table is stored in a flat file, like a CSV file
#
# 3 - based on the example given in the assignment statement, the info about a
# flight is found on consecutive lines.
#

Fare_Classes_List = ["Q", "X", "V", "Y"]
Fare_Classes_Set = set(Fare_Classes_List)


def next_class(c):
    if c == "Q":
        return "X"
    elif c == "X":
        return "V"
    elif c == "V":
        return "Y"
    else:
        None



def fetch_flight_info(given_flight_id):
    """
    Returns a dictionary of the following form:
    {
        "flight_id": flight_id,
        "Q": {
               fare_class_rank: <fare class rank value>,
               fare_value: <fare value>,
               booking: <number of booked seats>
        },
        "X": {
               fare_class_rank: <fare class rank value>,
               fare_value: <fare value>,
               booking: <number of booked seats>
        },
        "V": {
               fare_class_rank: <fare class rank value>,
               fare_value: <fare value>,
               booking: <number of booked seats>
        },
        "Y": {
               fare_class_rank: <fare class rank value>,
               fare_value: <fare value>,
               booking: <number of booked seats>
        },
    }
    """
    info = {
        "flight_id": given_flight_id,
        "encountered fare classes": [],
    }
    cur_dir = os.path.dirname(os.path.realpath(__file__))
    with open(os.path.join(cur_dir, "../data/flat_file.txt"), "r") as fh:
        while True:
            line = fh.readline()
            flight_id, fare_class, fare_class_rank, fare_value, booking = line.split(";")
            flight_id = flight_id.strip()
            if given_flight_id == flight_id:
                fare_class = fare_class.strip()
                info["encountered fare classes"].append(fare_class)
                info[fare_class] = {
                    "fare class rank": int(fare_class_rank),
                    "fare value": float(fare_value),
                    "booking": int(booking),
                }
                if Fare_Classes_Set == set(info["encountered fare classes"]):
                    break
    # Assumption - we have the data for the given flight id...
    return info


def compute_ratios(flight_info):
    """This answers the part 1 of the programming test, question 1.
    """
    ratios = {}
    for i in range(len(Fare_Classes_List) - 1):
        current_fc = Fare_Classes_List[i]
        next_fc = Fare_Classes_List[i+1]
        ratios[current_fc] = flight_info[next_fc]["fare value"] / flight_info[current_fc]["fare value"]
    return ratios


def compute_booking_ratios(flight_info):
    """This answers the part 2 of the programming test, question 1.
    """
    total = 0.0
    for fare_class in Fare_Classes_List:
        total += float(flight_info[fare_class]["booking"])
    for fare_class in Fare_Classes_List:
        ratio = 100.0 * flight_info[fare_class]["booking"] / total
        flight_info[fare_class]["booking ratio"] = ratio
    return flight_info


if __name__ == "__main__":
    d = fetch_flight_info("YULCDG215136")
    print(d)
